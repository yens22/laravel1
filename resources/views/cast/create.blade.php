@extends('layout.master')
@section('judul')
Tambah Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Cast : </label>
      <input type="text" name="nama" class="form-control" >
      </div>
      @error('Nama Cast')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label>Umur Cast : </label>
      <input type="text" name="umur" class="form-control" >
    </div>
    @error('Umur Cast')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio Cast : </label>
        <textarea name="bio" class="form-control"></textarea>
      </div>
      @error('Bio Cast')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection