<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','indexcontroller@index');
Route::get('/registasi','authcontroller@regis');
Route::post('/welcome','authcontroller@kirim');
Route::get('/tabel',function(){
    return view('table.data-table');
});
//CRUD kategori
//creat
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');

//read
Route::get('/cast','CastController@index');
Route::get('/cast/{cast_id}','CastController@show');

//update
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');

//delete
Route::delete('/cast/{casst_id}','CastController@destroy' );